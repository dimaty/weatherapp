package com.bushuev.dmitriy.weatherapp.di.module

import android.app.Application
import android.arch.persistence.room.Room
import com.bushuev.dmitriy.data.weather.local.database.WeatherDao
import com.bushuev.dmitriy.data.weather.local.database.WeatherDatabase
import com.bushuev.dmitriy.weatherapp.di.scope.AppScope
import dagger.Module
import dagger.Provides

/**
 * Contains dependencies for working with database
 *
 * @author Dmitriy Bushuev.
 */
@Module
class DatabaseModule {

    @AppScope
    @Provides
    fun chatDatabase(context: Application): WeatherDatabase =
            Room.databaseBuilder(context, WeatherDatabase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

    @AppScope
    @Provides
    fun conversationsDao(chatDatabase: WeatherDatabase): WeatherDao =
            chatDatabase.weatherDao()

    companion object {
        private const val DATABASE_NAME = "weatherDatabase"
    }

}