package com.bushuev.dmitriy.weatherapp.di.scope

import javax.inject.Scope
import javax.inject.Singleton

/**
 * Scope of main application screen dependencies
 *
 * @author Dmitriy Bushuev
 */
@Scope
@Singleton
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope