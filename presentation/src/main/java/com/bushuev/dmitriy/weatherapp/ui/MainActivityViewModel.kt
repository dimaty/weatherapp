package com.bushuev.dmitriy.weatherapp.ui

import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.domain.usecase.GetWeatherByLocation
import com.bushuev.dmitriy.weatherapp.ui.support.ReducerSupportStreamViewModel
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * View model for main application screen
 *
 * <p>
 *     Controls all requests and state of connected screen
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class MainActivityViewModel @Inject constructor(private val getWeatherByLocation: GetWeatherByLocation)
    : ReducerSupportStreamViewModel<WeatherUiEvent, WeatherState, MainActivityViewModel.WeatherResult>() {

    override val initialState: WeatherState = WeatherState()

    private val loadWeatherTransformer: ObservableTransformer<WeatherUiEvent.LoadCurrentWeather, WeatherResult> =
            ObservableTransformer { events ->
                events.flatMap {
                    load()
                }
            }

    init {
        registerReducer(WeatherResult.LoadingResult::class.java,
                { state, result -> state.copy(loading = (result as WeatherResult.LoadingResult).loading, error = null) })

        registerReducer(WeatherResult.WeatherLoadResult::class.java,
                { state, result ->
                    state.copy(loading = false,
                            error = (result as WeatherResult.WeatherLoadResult).error,
                            weather = result.weather)
                })

        register(WeatherUiEvent::class.java, loadWeatherTransformer)

        subscribe()
    }

    private fun load() =
            getWeatherByLocation.execute()
                    .map { processResult(it) }
                    .startWith(WeatherResult.LoadingResult(true))

    private fun processResult(result: Result<WeatherGroup>): WeatherResult =
            WeatherResult.WeatherLoadResult(result.data ?: WeatherGroup(), result.error)

    /**
     * Result of communication with weather data sources
     */
    sealed class WeatherResult {

        class LoadingResult(val loading: Boolean) : WeatherResult()
        class WeatherLoadResult(val weather: WeatherGroup, val error: Throwable?) : WeatherResult()

    }

}