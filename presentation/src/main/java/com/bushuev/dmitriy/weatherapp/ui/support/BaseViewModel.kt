package com.bushuev.dmitriy.weatherapp.ui.support

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Base implementation of [ViewModel] that controls all existing subscriptions [Disposable] end unsubscribes in [onCleared]
 *
 * @author Dmitriy Bushuev
 */
abstract class BaseViewModel : ViewModel() {

    private val subscriptions = CompositeDisposable()

    protected fun addSubscription(subscription: Disposable) {
        this.subscriptions.add(subscription)
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}
