package com.bushuev.dmitriy.weatherapp

import com.bushuev.dmitriy.weatherapp.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 * Root context of application
 *
 * <p>
 *     Initialize all necessary dependencies and entities required to project
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
class AndroidApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<AndroidApplication> =
            DaggerApplicationComponent.builder().application(this).build()

}
