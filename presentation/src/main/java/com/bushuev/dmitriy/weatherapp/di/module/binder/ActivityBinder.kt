package com.bushuev.dmitriy.weatherapp.di.module.binder

import com.bushuev.dmitriy.weatherapp.di.module.LocationModule
import com.bushuev.dmitriy.weatherapp.di.module.WeatherModule
import com.bushuev.dmitriy.weatherapp.di.module.factory.MainActivityFactoryModule
import com.bushuev.dmitriy.weatherapp.di.scope.MainScope
import com.bushuev.dmitriy.weatherapp.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module for building subcomponents for all root screens (activities) that require dependencies
 *
 * <p>
 *      Every subcomponent should  declare as more specific modules as possible
 *      to avoid providing of unnecessary dependencies
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
@Module
abstract class ActivityBinder {

    @MainScope
    @ContributesAndroidInjector(modules = [MainActivityFactoryModule::class, WeatherModule::class, LocationModule::class])
    abstract fun bindMainActivity(): MainActivity

}