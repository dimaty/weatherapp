package com.bushuev.dmitriy.weatherapp.di.module

import android.app.Application
import com.bushuev.dmitriy.data.ApplicationProperties
import com.bushuev.dmitriy.weatherapp.di.scope.AppScope
import dagger.Module
import dagger.Provides

/**
 * Contains global application dependencies
 *
 * @author Dmitriy Bushuev
 */
@Module
class ApplicationModule {

    @AppScope
    @Provides
    internal fun properties(context: Application): ApplicationProperties =
            ApplicationProperties(context)

}
