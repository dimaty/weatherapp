package com.bushuev.dmitriy.weatherapp.ui.support

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Extension of [RecyclerView.ViewHolder] with ability of binding value
 *
 * @author Dmitriy Bushuev
 */
abstract class BindableViewHolder<in T>(view: View) : RecyclerView.ViewHolder(view) {

    /**
     * Process required item
     */
    abstract fun bind(value: T)

}