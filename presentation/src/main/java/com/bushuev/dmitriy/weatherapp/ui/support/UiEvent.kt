package com.bushuev.dmitriy.weatherapp.ui.support

/**
 * Interface-marker for all UI events
 *
 * @author Dmitriy Bushuev.
 */
interface UiEvent
