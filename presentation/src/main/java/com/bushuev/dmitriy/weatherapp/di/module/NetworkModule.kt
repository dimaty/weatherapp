package com.bushuev.dmitriy.weatherapp.di.module

import com.bushuev.dmitriy.data.ApplicationProperties
import com.bushuev.dmitriy.data.weather.remote.WeatherResponse
import com.bushuev.dmitriy.data.weather.remote.network.WeatherDeserializer
import com.bushuev.dmitriy.data.weather.remote.network.WeatherGroupDeserializer
import com.bushuev.dmitriy.data.weather.remote.network.interceptor.ApiKeyRequestInterceptor
import com.bushuev.dmitriy.data.weather.remote.network.interceptor.DaysCountRequestInterceptor
import com.bushuev.dmitriy.data.weather.remote.network.interceptor.UnitsInterceptor
import com.bushuev.dmitriy.weatherapp.di.scope.AppScope
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Contains base network dependencies
 *
 * @author Dmitriy Bushuev.
 */
@Module
class NetworkModule {

    @AppScope
    @Provides
    internal fun entityConverterFactory(): Converter.Factory =
            GsonConverterFactory.create(
                    GsonBuilder()
                            .registerTypeAdapter(WeatherResponse::class.java, WeatherGroupDeserializer(WeatherDeserializer()))
                            .setLenient()
                            .create())

    @AppScope
    @Provides
    internal fun httpClient(interceptors: Set<@JvmSuppressWildcards Interceptor>): OkHttpClient =
            OkHttpClient.Builder().apply {
                interceptors.forEach { addInterceptor(it) }
            }.build()

    @AppScope
    @IntoSet
    @Provides
    internal fun apiKeyRequestInterceptor(applicationProperties: ApplicationProperties): Interceptor =
            ApiKeyRequestInterceptor(applicationProperties)

    @AppScope
    @IntoSet
    @Provides
    internal fun unitsRequestInterceptor(): Interceptor =
            UnitsInterceptor()

    @AppScope
    @IntoSet
    @Provides
    internal fun daysCountInterceptor(applicationProperties: ApplicationProperties): Interceptor =
            DaysCountRequestInterceptor(applicationProperties)

}
