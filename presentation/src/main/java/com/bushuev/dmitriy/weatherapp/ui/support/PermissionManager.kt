package com.bushuev.dmitriy.weatherapp.ui.support

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

/**
 * Controller of permission requesting process
 *
 * @author Dmitriy Bushuev
 */
class PermissionManager(private val activity: Activity,
                        private val requestCode: Int,
                        private val onSuccess: (() -> Unit)? = null,
                        private val onFailure: (() -> Unit)? = null) {

    /**
     * Request required permissions
     *
     * <p>
     *     If all permission already granted - call [onSuccess]
     * </p>
     */
    fun requestPermissions(permissions: Array<String>) {
        if (permissionsGranted(permissions)) {
            onSuccess?.invoke()
            return
        }

        requestPermissionsInternal(permissions)
    }

    private fun requestPermissionsInternal(permissions: Array<String>) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode)
    }

    /**
     * Check whether all permissions already granted
     */
    fun permissionsGranted(permissions: Array<String>): Boolean {
        for (permission in permissions) {
            if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    /**
     * Callback for finishing requesting process
     */
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (this.requestCode == requestCode) {
            when {
                grantResults.contains(PackageManager.PERMISSION_DENIED) -> onFailure?.invoke()
                else -> onSuccess?.invoke()
            }
        }
    }

    /**
     * Builder for permissions manager
     */
    class Builder(private val activity: Activity) {

        private var requestCode: Int = 1
        private var onSuccess: (() -> Unit)? = null
        private var onFailure: (() -> Unit)? = null

        fun requestCode(requestCode: Int): Builder {
            this.requestCode = requestCode
            return this
        }

        fun onSuccess(action: () -> Unit): Builder {
            this.onSuccess = action
            return this
        }

        fun onFailure(action: () -> Unit): Builder {
            this.onFailure = action
            return this
        }

        fun build() = PermissionManager(activity, requestCode, onSuccess, onFailure)

    }

    companion object {
        fun with(activity: Activity) =
                PermissionManager.Builder(activity)
    }

}