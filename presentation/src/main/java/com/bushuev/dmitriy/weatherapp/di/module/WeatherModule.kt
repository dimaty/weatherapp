package com.bushuev.dmitriy.weatherapp.di.module

import com.bushuev.dmitriy.data.weather.WeatherRepositoryImpl
import com.bushuev.dmitriy.data.weather.local.WeatherLocalDataSource
import com.bushuev.dmitriy.data.weather.local.WeatherLocalDataSourceImpl
import com.bushuev.dmitriy.data.weather.local.database.WeatherDao
import com.bushuev.dmitriy.data.weather.remote.WeatherRemoteDataSource
import com.bushuev.dmitriy.data.weather.remote.WeatherRemoteDataSourceImpl
import com.bushuev.dmitriy.data.weather.remote.WeatherService
import com.bushuev.dmitriy.data.weather.remote.network.ServiceFactory
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.domain.repository.WeatherRepository
import com.bushuev.dmitriy.weatherapp.di.scope.MainScope
import dagger.Module
import dagger.Provides
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Dependencies for main application screen
 *
 * @author Dmitriy Bushuev.
 */
@Module
class WeatherModule {

    @MainScope
    @Provides
    fun weatherRepository(localDataSource: WeatherLocalDataSource, remoteDataSource: WeatherRemoteDataSource): WeatherRepository =
            WeatherRepositoryImpl(localDataSource, remoteDataSource)

    @MainScope
    @Provides
    fun weatherLocalDataSource(weatherDao: WeatherDao): WeatherLocalDataSource =
            WeatherLocalDataSourceImpl(weatherDao)

    @MainScope
    @Provides
    fun weatherRemoteDataSource(weatherService: WeatherService): WeatherRemoteDataSource =
            WeatherRemoteDataSourceImpl(weatherService)

    @MainScope
    @Provides
    fun weatherRemoteService(serviceFactory: ServiceFactory): WeatherService =
            serviceFactory.createService(WeatherService::class)

    @MainScope
    @Provides
    fun weatherLoadSchedulerTransformer(): ObservableTransformer<Result<WeatherGroup>, Result<WeatherGroup>> =
            ObservableTransformer { upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }

}