package com.bushuev.dmitriy.weatherapp.di.scope

import javax.inject.Scope
import javax.inject.Singleton

/**
 * Describes global application scope for dependencies
 *
 * @author Dmitriy Bushuev
 */
@Scope
@Singleton
@Retention(AnnotationRetention.RUNTIME)
annotation class AppScope
