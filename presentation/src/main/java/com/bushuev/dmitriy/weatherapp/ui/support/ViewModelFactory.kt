package com.bushuev.dmitriy.weatherapp.ui.support

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bushuev.dmitriy.weatherapp.utils.Assertion
import javax.inject.Inject
import javax.inject.Provider

/**
 * Universal factory for [ViewModel] required to inject parameters to ViewModel constructor
 *
 * @author Dmitriy Bushuev.
 */
class ViewModelFactory
@Inject constructor(private val providers: Map<Class<out ViewModel>,
        @JvmSuppressWildcards Provider<ViewModel>>) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var provider: Provider<out ViewModel>? = providers[modelClass]

        if (provider == null) {
            for (key in providers.keys) {
                if (modelClass.isAssignableFrom(key)) {
                    provider = providers[key]
                }
            }
        }

        Assertion.checkNotNull(provider, "Unknown model type {$modelClass.name}")

        return provider!!.get() as T
    }
}