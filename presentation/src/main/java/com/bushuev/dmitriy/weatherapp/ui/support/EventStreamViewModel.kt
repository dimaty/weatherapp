package com.bushuev.dmitriy.weatherapp.ui.support

import android.arch.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.subjects.BehaviorSubject

/**
 * Base implementation of event streaming MVI-style view model
 *
 * <p>
 *     Usage:
 *     1) Define initial state by overriding abstract [initialState]
 *     2) Override [reduce] function for [Result] to [State] transformation
 *     3) Register all required ui event types with their transformers from [Event] to [Result]
 *     4) Call [subscribe] to init main ui events transformer with registered types
 *
 *     Example:
 *           register(WeatherUiEvent::class.java, loadWeatherTransformer)
 *           subscribe()
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
abstract class EventStreamViewModel<Event : UiEvent, State, Result> : BaseViewModel() {

    protected abstract val initialState: State

    val uiEvents: BehaviorSubject<Event> = BehaviorSubject.create()
    val uiState = MutableLiveData<State>()

    private val eventTransformerRegistry: MutableMap<Class<Event>, ObservableTransformer<Event, Result>> = hashMapOf()

    /**
     * Base UI events stream transformer that shared this event's between all consumers
     *
     * <p>
     *     [skip(1)] - skips emitting first item (initial item for scan accumulator)
     * </p>
     */
    private val mainTransformer: ObservableTransformer<Event, State> =
            ObservableTransformer { events ->
                events.publish { shared ->
                    Observable.merge(
                            createMergedObservableList(shared)
                    )
                }.scan(initialState, { previousState, result -> reduce(previousState, result) }).skip(1)
            }

    /**
     * Init main transformer of events stream
     */
    protected fun subscribe() {
        addSubscription(uiEvents.compose(mainTransformer)
                .subscribe({ state -> uiState.value = state }))
    }

    /**
     * Merges all registered event types into the list of observables
     */
    private fun createMergedObservableList(shared: Observable<Event>): List<Observable<Result>> {
        val observables = arrayListOf<Observable<Result>>()

        for ((type, transformer) in eventTransformerRegistry) {
            observables += shared.ofType(type).compose(transformer)
        }

        return observables
    }

    /**
     * Transforms previous screen state using view intent result
     */
    protected abstract fun reduce(previousState: State, result: Result): State

    /**
     * Register observable transformer by ui event type
     */
    fun register(event: Class<out Event>, transformer: ObservableTransformer<out Event, Result>) {
        eventTransformerRegistry[event as Class<Event>] = transformer as ObservableTransformer<Event, Result>
    }

}