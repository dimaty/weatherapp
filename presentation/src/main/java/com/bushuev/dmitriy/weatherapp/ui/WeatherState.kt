package com.bushuev.dmitriy.weatherapp.ui

import com.bushuev.dmitriy.domain.entity.WeatherGroup

/**
 * Describe state of weather screen
 *
 * <p>
 *     [loading: Boolean] - Loading state of screen
 *     [error: Throwable?] - Error state of screen
 *     [weather: List<Weather>] - List of weather information
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
data class WeatherState(val loading: Boolean = false,
                        val error: Throwable? = null,
                        val weather: WeatherGroup = WeatherGroup())