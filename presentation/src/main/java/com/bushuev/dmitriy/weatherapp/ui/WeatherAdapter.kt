package com.bushuev.dmitriy.weatherapp.ui

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bushuev.dmitriy.data.utils.DateToStringConverter
import com.bushuev.dmitriy.data.utils.DateToTodayStringConverter
import com.bushuev.dmitriy.domain.entity.Weather
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.weatherapp.R
import com.bushuev.dmitriy.weatherapp.ui.support.BindableViewHolder
import com.bushuev.dmitriy.weatherapp.utils.inflate

/**
 * Adapter for list of weather items
 *
 * <p>
 *     Contains two types of holders - for main item, describing today weather forecast,
 *     and simple items for other days
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class WeatherAdapter : RecyclerView.Adapter<BindableViewHolder<Weather>>() {

    private var items: MutableList<Weather> = arrayListOf()

    private var cityName: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindableViewHolder<Weather> =
            when (viewType) {
                ITEM -> WeatherHolder(parent)
                else -> WeatherHeaderHolder(parent)
            }

    override fun onBindViewHolder(holder: BindableViewHolder<Weather>, position: Int) =
            holder.bind(items[position])

    override fun getItemCount(): Int =
            items.size

    override fun getItemViewType(position: Int): Int =
            when (position) {
                0 -> HEADER
                else -> ITEM
            }

    fun setData(weather: WeatherGroup) {
        items.apply {
            clear()
            addAll(weather.weather)
        }

        this.cityName = weather.city ?: ""

        notifyDataSetChanged()
    }

    inner class WeatherHeaderHolder(parent: ViewGroup)
        : BindableViewHolder<Weather>(inflate(parent, R.layout.weather_header_layout)) {

        @BindView(R.id.icon)
        internal lateinit var icon: ImageView

        @BindView(R.id.city)
        internal lateinit var city: TextView

        @BindView(R.id.date)
        internal lateinit var date: TextView

        @BindView(R.id.temperature)
        internal lateinit var temperature: TextView

        @BindView(R.id.description)
        internal lateinit var description: TextView

        @BindView(R.id.speed)
        internal lateinit var speed: TextView

        @BindView(R.id.humidity)
        internal lateinit var humidity: TextView

        @BindView(R.id.pressure)
        internal lateinit var pressure: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun bind(value: Weather) {
            icon.setImageResource(WeatherUtils.getArtResourceForWeatherCondition(value.weatherType))
            city.text = cityName
            description.text = value.description

            with(itemView.context) {
                date.text = DateToTodayStringConverter(today = getString(R.string.today)).convert(value.date)
                temperature.text = getString(R.string.temperature, value.temperature)
                speed.text = getString(R.string.wind, value.speed)
                humidity.text = getString(R.string.humidity, value.humidity)
                pressure.text = getString(R.string.pressure, value.pressure)
            }

        }

    }

    inner class WeatherHolder(parent: ViewGroup)
        : BindableViewHolder<Weather>(inflate(parent, R.layout.weather_item_layout)) {

        @BindView(R.id.icon)
        internal lateinit var icon: ImageView

        @BindView(R.id.date)
        internal lateinit var date: TextView

        @BindView(R.id.temperature)
        internal lateinit var temperature: TextView

        @BindView(R.id.description)
        internal lateinit var description: TextView

        @BindView(R.id.speed)
        internal lateinit var speed: TextView

        @BindView(R.id.humidity)
        internal lateinit var humidity: TextView

        @BindView(R.id.pressure)
        internal lateinit var pressure: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        override fun bind(value: Weather) {
            icon.setImageResource(WeatherUtils.getIconResourceForWeatherCondition(value.weatherType))
            date.text = DateToStringConverter(DATE_FORMAT).convert(value.date)
            description.text = value.description

            with(itemView.context) {
                temperature.text = getString(R.string.temperature, value.temperature)
                speed.text = getString(R.string.wind, value.speed)
                humidity.text = getString(R.string.humidity, value.humidity)
                pressure.text = getString(R.string.pressure, value.pressure)
            }
        }

    }

    companion object {
        private const val ITEM = 1
        private const val HEADER = 2

        private const val DATE_FORMAT = "dd.MM"
    }

}
