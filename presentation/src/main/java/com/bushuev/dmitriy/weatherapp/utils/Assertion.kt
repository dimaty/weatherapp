package com.bushuev.dmitriy.weatherapp.utils

/**
 * Contains all base precondition functions
 *
 * @author Dmitriy Bushuev
 */
object Assertion {

    /**
     * Throws [IllegalArgumentException] if input value is null
     *
     * @param target Value to nullchecking
     * @param errorMessage Exception message
     */
    fun checkNotNull(target: Any?, errorMessage: String) {
        if (target == null) {
            throw IllegalArgumentException(errorMessage)
        }
    }

}
