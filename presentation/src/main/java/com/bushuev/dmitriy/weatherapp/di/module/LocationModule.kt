package com.bushuev.dmitriy.weatherapp.di.module

import android.app.Application
import com.bushuev.dmitriy.data.location.LocationRepositoryImpl
import com.bushuev.dmitriy.data.location.base.RxLocationProvider
import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.repository.LocationRepository
import com.bushuev.dmitriy.weatherapp.di.scope.MainScope
import dagger.Module
import dagger.Provides
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Contains dependencies for working with location
 *
 * @author Dmitriy Bushuev.
 */
@Module
class LocationModule {

    @MainScope
    @Provides
    fun locationRepository(locationProvider: RxLocationProvider): LocationRepository =
            LocationRepositoryImpl(locationProvider)

    @MainScope
    @Provides
    fun locationProvider(context: Application): RxLocationProvider =
            RxLocationProvider(context)

    @MainScope
    @Provides
    fun weatherLoadSchedulerTransformer(): SingleTransformer<Result<Location>, Result<Location>> =
            SingleTransformer { upstream -> upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()) }

}