package com.bushuev.dmitriy.weatherapp.ui

import android.Manifest
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.weatherapp.R
import com.bushuev.dmitriy.weatherapp.ui.support.PermissionManager
import com.bushuev.dmitriy.weatherapp.ui.support.ViewModelFactory
import com.bushuev.dmitriy.weatherapp.utils.provideViewModel
import com.bushuev.dmitriy.weatherapp.utils.setVisibility
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Root screen of application
 *
 * @author Dmitriy Bushuev
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    @BindView(R.id.weather)
    internal lateinit var weatherList: RecyclerView

    @BindView(R.id.refresh)
    internal lateinit var refresh: SwipeRefreshLayout

    @BindView(R.id.progress)
    internal lateinit var progress: ProgressBar

    @BindView(R.id.emptyView)
    internal lateinit var emptyView: View

    private lateinit var viewModel: MainActivityViewModel
    private val weatherAdapter: WeatherAdapter by lazy { WeatherAdapter() }

    private val permissionManager: PermissionManager by lazy {
        PermissionManager.with(this)
                .onSuccess {
                    viewModel.uiEvents.onNext(WeatherUiEvent.LoadCurrentWeather())
                }
                .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        initList()

        viewModel = provideViewModel(viewModelFactory) {
            uiState.observe(this@MainActivity, Observer { applyState(it) })
        }

        permissionManager.requestPermissions(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))

        refresh.setOnRefreshListener {
            viewModel.uiEvents.onNext(WeatherUiEvent.LoadCurrentWeather())
            refresh.isRefreshing = false
        }
    }

    private fun initList() = with(weatherList) {
        layoutManager = LinearLayoutManager(this@MainActivity)
        adapter = weatherAdapter
    }

    private fun applyState(state: WeatherState?) {
        setData(state?.weather ?: WeatherGroup(), needShowEmptyView(state ?: WeatherState()))
        showProgress(state?.loading ?: false)
        showError(state?.error)
    }

    private fun needShowEmptyView(state: WeatherState) =
            !state.loading && state.weather.weather.isEmpty()

    private fun setData(data: WeatherGroup, showEmpty: Boolean) {
        weatherAdapter.setData(data)
        setVisibility(showEmpty, emptyView)
    }

    private fun showProgress(state: Boolean) = with(state) {
        setVisibility(this, progress)
        refresh.isEnabled = !this
    }

    private fun showError(error: Throwable?) = error?.let {
        Toast.makeText(this, R.string.error_text, Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) =
            permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)

}
