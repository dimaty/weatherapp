package com.bushuev.dmitriy.weatherapp.ui.support

/**
 * Extension of [EventStreamViewModel] with ability to define state reducers by event type
 *
 * <p>
 *     Usage:
 *     Register all required state reducers for all result types
 *
 *     Example:
 *          registerReducer(WeatherResult.LoadingResult::class.java,
 *           { state, result -> state.copy(loading = (result as WeatherResult.LoadingResult).loading) })
 *
 *           registerReducer(WeatherResult.WeatherLoadResult::class.java,
 *           { state, result ->
 *                  state.copy(loading = false,
 *                  error = (result as WeatherResult.WeatherLoadResult).error,
 *                  weather = result.weather)
 *           })
 *
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
abstract class ReducerSupportStreamViewModel<Event : UiEvent, State, Result : Any> : EventStreamViewModel<Event, State, Result>() {

    private val reducerRegistry: MutableMap<Class<out Result>, (State, Result) -> State> = HashMap()

    override fun reduce(previousState: State, result: Result): State =
            reducerRegistry[result::class.java]!!(previousState, result)

    /**
     * Register state reducer by result type
     */
    fun registerReducer(result: Class<out Result>, reducer: (State, Result) -> State) {
        reducerRegistry[result] = reducer
    }

}
