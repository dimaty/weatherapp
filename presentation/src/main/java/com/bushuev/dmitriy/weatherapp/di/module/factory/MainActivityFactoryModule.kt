package com.bushuev.dmitriy.weatherapp.di.module.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.bushuev.dmitriy.weatherapp.ui.MainActivityViewModel
import com.bushuev.dmitriy.weatherapp.di.ViewModelKey
import com.bushuev.dmitriy.weatherapp.di.scope.MainScope
import com.bushuev.dmitriy.weatherapp.ui.support.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Provides [ViewModelFactory] dependency to main application screen
 *
 * @author Dmitriy Bushuev.
 */
@Module
interface MainActivityFactoryModule {

    @MainScope
    @Binds
    fun viewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @MainScope
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun mainViewModelMap(viewModel: MainActivityViewModel): ViewModel

}