package com.bushuev.dmitriy.weatherapp.di

import android.app.Application
import com.bushuev.dmitriy.weatherapp.AndroidApplication
import com.bushuev.dmitriy.weatherapp.di.module.ApplicationModule
import com.bushuev.dmitriy.weatherapp.di.module.DatabaseModule
import com.bushuev.dmitriy.weatherapp.di.module.NetworkModule
import com.bushuev.dmitriy.weatherapp.di.module.binder.ActivityBinder
import com.bushuev.dmitriy.weatherapp.di.scope.AppScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Enter point of dependency injection process. Contains higher order dependencies of all app
 *
 * @author Dmitriy Bushuev.
 */
@AppScope
@Component(modules = [AndroidSupportInjectionModule::class,
    ActivityBinder::class,
    DatabaseModule::class,
    NetworkModule::class,
    ApplicationModule::class])
interface ApplicationComponent : AndroidInjector<AndroidApplication> {

    override fun inject(instance: AndroidApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

}