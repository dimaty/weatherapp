package com.bushuev.dmitriy.weatherapp.ui

import com.bushuev.dmitriy.weatherapp.ui.support.UiEvent

/**
 * Weather screen UI events
 *
 * @author Dmitriy Bushuev.
 */
sealed class WeatherUiEvent : UiEvent {

    /**
     * Request of weather loading
     */
    class LoadCurrentWeather : WeatherUiEvent()

}