package com.bushuev.dmitriy.weatherapp.utils

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * See library javadock of [inflate] function in [LayoutInflater] class
 */
fun <T : View> inflate(parent: ViewGroup, layout: Int, attach: Boolean = false): T =
        LayoutInflater.from(parent.context).inflate(layout, parent, attach) as T

fun <T : View> inflate(context: Context, layoutId: Int): T {
    return LayoutInflater.from(context).inflate(layoutId, null) as T
}

/**
 * Provide implementation of [ViewModel], connected with receiver FragmentActivity
 */
inline fun <reified T : ViewModel> FragmentActivity.provideViewModel(factory: ViewModelProvider.Factory) =
        ViewModelProviders.of(this, factory).get(T::class.java)

/**
 * Provide implementation of [ViewModel], connected with receiver FragmentActivity and apply required action
 */
inline fun <reified T : ViewModel> FragmentActivity.provideViewModel(factory: ViewModelProvider.Factory, action: T.() -> Unit) =
        ViewModelProviders.of(this, factory).get(T::class.java).apply(action)

/**
 * Change visibility of required views according to input flag
 */
fun setVisibility(visible: Boolean, vararg views: View) {
    for (view in views) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}