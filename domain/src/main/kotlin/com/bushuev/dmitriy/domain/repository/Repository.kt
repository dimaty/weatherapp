package com.bushuev.dmitriy.domain.repository

/**
 * Interface-marker for defining repository implementations
 *
 * @author Dmitriy Bushuev.
 */
interface Repository
