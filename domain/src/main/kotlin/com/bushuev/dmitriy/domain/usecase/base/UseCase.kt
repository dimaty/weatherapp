package com.bushuev.dmitriy.domain.usecase.base

/**
 * Describes single action, connected with application's business logic
 *
 * @author Dmitriy Bushuev.
 */
interface UseCase<in I, out O> {

    /**
     * Starting execution process
     *
     * @param param I Input params, that required for execution
     * @return O Returning value
     */
    fun execute(param: I? = null): O

}