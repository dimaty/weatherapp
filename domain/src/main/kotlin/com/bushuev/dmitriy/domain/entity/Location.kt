package com.bushuev.dmitriy.domain.entity

/**
 * Describes location required to request weather
 *
 * @author Dmitriy Bushuev
 */
data class Location(val latitude: Double, val longitude: Double)