package com.bushuev.dmitriy.domain.entity

import java.util.*

/**
 * Describe weather entity
 *
 * @author Dmitriy Bushuev
 */
data class Weather(val date: Date,
                   val temperature: Double,
                   val pressure: Double,
                   val humidity: Double,
                   val speed: Double,
                   val description: String,
                   val weatherType: Int)