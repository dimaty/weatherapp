package com.bushuev.dmitriy.domain.usecase.base

import com.bushuev.dmitriy.domain.repository.Repository

/**
 * Base implementation of [RepositoryUseCase] with functionality of defining repository
 *
 * @author Dmitriy Bushuev.
 */
abstract class BaseRepositoryUseCase<in I, out O, R : Repository>(override var repository: R)
    : RepositoryUseCase<I, O, R>