package com.bushuev.dmitriy.domain.entity

/**
 * Wrapper for all result data coming from repository or other source
 *
 * @author Dmitriy Bushuev.
 */
data class Result<out T>(val data: T? = null, val error: Throwable? = null)