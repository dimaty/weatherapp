package com.bushuev.dmitriy.domain.usecase

import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.domain.repository.WeatherRepository
import com.bushuev.dmitriy.domain.usecase.base.BaseRepositoryUseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import javax.inject.Inject

/**
 * Use case for retrieving weather list from [WeatherRepository]
 *
 * <p>
 *    Apply [SingleTransformer] to upstream to define schedulers for execution process
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
class GetWeatherByLocation
@Inject constructor(repository: WeatherRepository,
                    private val getLastLocationUseCase: GetLastLocationUseCase,
                    private val schedulerTransformer: ObservableTransformer<Result<WeatherGroup>, Result<WeatherGroup>>) :
        BaseRepositoryUseCase<Unit, Observable<Result<WeatherGroup>>, WeatherRepository>(repository) {

    override fun execute(param: Unit?): Observable<Result<WeatherGroup>> =
            getLastLocation().flatMap { location ->
                repository.getWeather(location.data)
                        .toObservable()
                        .compose(schedulerTransformer)
            }

    private fun getLastLocation() =
            getLastLocationUseCase.execute().toObservable()

}