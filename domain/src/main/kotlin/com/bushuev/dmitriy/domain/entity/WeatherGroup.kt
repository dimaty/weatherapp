package com.bushuev.dmitriy.domain.entity

/**
 * Wrapper for list of [Weather] and sity name
 *
 * @author Dmitriy Bushuev
 */
data class WeatherGroup(val city: String? = "",
                        val weather: List<Weather> = arrayListOf())