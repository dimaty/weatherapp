package com.bushuev.dmitriy.domain.usecase.base

import com.bushuev.dmitriy.domain.repository.Repository


/**
 * Extension of [UseCase] interface that provides ability to define specific [Repository] connected with usecase
 *
 * @author Dmitriy Bushuev.
 */
interface RepositoryUseCase<in I, out O, R : Repository> : UseCase<I, O> {

    /**
     * Specific usecase repository
     */
    var repository: R

}