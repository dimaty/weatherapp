package com.bushuev.dmitriy.domain.usecase

import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.repository.LocationRepository
import com.bushuev.dmitriy.domain.usecase.base.BaseRepositoryUseCase
import io.reactivex.Single
import io.reactivex.SingleTransformer
import javax.inject.Inject

/**
 * Use case for retrieving last known user location from [LocationRepository]
 *
 * <p>
 *    Apply [SingleTransformer] to upstream to define schedulers for execution process
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
class GetLastLocationUseCase
@Inject constructor(repository: LocationRepository,
                    private val schedulerTransformer: SingleTransformer<Result<Location>, Result<Location>>) :
        BaseRepositoryUseCase<Unit, Single<Result<Location>>, LocationRepository>(repository) {

    override fun execute(param: Unit?): Single<Result<Location>> =
            repository.getLastLocation().compose(schedulerTransformer)

}