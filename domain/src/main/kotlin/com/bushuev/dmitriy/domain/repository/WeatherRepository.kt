package com.bushuev.dmitriy.domain.repository

import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.entity.Weather
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import io.reactivex.Single

/**
 * Describes functionality of weather data source
 */
interface WeatherRepository : Repository {

    /**
     * Retrieve weather by required location
     */
    fun getWeather(location: Location?): Single<Result<WeatherGroup>>

}