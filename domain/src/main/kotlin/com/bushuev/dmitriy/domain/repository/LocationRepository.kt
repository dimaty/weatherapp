package com.bushuev.dmitriy.domain.repository

import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Describes functionality of location data source
 *
 * @author Dmitriy Bushuev
 */
interface LocationRepository : Repository {

    /**
     * Retrieve [Maybe] with last known location
     */
    fun getLastLocation(): Single<Result<Location>>

}