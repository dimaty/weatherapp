package com.bushuev.dmitriy.data.weather.remote.network

import com.bushuev.dmitriy.data.weather.remote.WeatherRemote
import com.bushuev.dmitriy.data.weather.remote.WeatherResponse
import com.google.gson.*
import java.lang.reflect.Type

/**
 * Deserializer for [WeatherResponse]
 *
 * <p>
 *     Specific deserializer for array of [WeatherRemote] required
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class WeatherGroupDeserializer<T : JsonDeserializer<Array<WeatherRemote>>>(private val deserializer: T)
    : JsonDeserializer<WeatherResponse> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): WeatherResponse {
        val gson = GsonBuilder().registerTypeAdapter(Array<WeatherRemote>::class.java, deserializer).setLenient().create()

        val jsonObject = json?.asJsonObject

        val city: String? = getCityName(getCity(jsonObject), gson)
        val weather: Array<WeatherRemote>? = getWeather(jsonObject, gson)

        return WeatherResponse(city ?: "", weather?.asList() ?: listOf())
    }

    private fun getCity(jsonObject: JsonObject?) = jsonObject?.getAsJsonObject(CITY_KEY)

    private fun getCityName(jsonObject: JsonObject?, gson: Gson) =
    // Workaround for unrecognized break space character. Replaced with unbreak space
            gson.fromJson(jsonObject?.get(CITY_NAME_KEY)?.asString?.replace("\u0020", "\u00A0"),
                    String::class.java)

    private fun getWeather(jsonObject: JsonObject?, gson: Gson) =
            gson.fromJson(jsonObject?.getAsJsonArray(ARRAY_KEY), Array<WeatherRemote>::class.java)

    companion object {
        private const val ARRAY_KEY = "list"
        private const val CITY_KEY = "city"
        private const val CITY_NAME_KEY = "name"
    }

}
