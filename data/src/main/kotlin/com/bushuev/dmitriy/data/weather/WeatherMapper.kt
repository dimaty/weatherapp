package com.bushuev.dmitriy.data.weather

import com.bushuev.dmitriy.data.utils.SecondsToDateConverter
import com.bushuev.dmitriy.data.weather.local.entity.WeatherLocal
import com.bushuev.dmitriy.data.weather.remote.WeatherRemote
import com.bushuev.dmitriy.domain.entity.Weather
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import java.util.*

/**
 * Mapper for weather entities from different app layers
 *
 * @author Dmitriy Bushuev
 */
object WeatherMapper {

    /**
     * Converts [WeatherLocal] to [Weather]
     */
    fun map(weatherLocal: WeatherLocal): Weather = with(weatherLocal) {
        Weather(Date(date),
                temperature,
                pressure,
                humidity,
                speed,
                description,
                weatherType)
    }

    /**
     * Converts [WeatherRemote] to [Weather]
     */
    fun map(weather: WeatherRemote): Weather = with(weather) {
        Weather(SecondsToDateConverter().convert(date),
                temperature,
                pressure,
                humidity,
                speed,
                description,
                weatherType)
    }

    /**
     * Converts [WeatherGroup] to [WeatherLocal]
     */
    fun map(weatherGroup: WeatherGroup): List<WeatherLocal> = with(weatherGroup) {
        weather.map {
            WeatherLocal(city ?: "",
                    it.date.time,
                    it.temperature,
                    it.pressure,
                    it.humidity,
                    it.speed,
                    it.description,
                    it.weatherType)
        }
    }

    /**
     * Converts [WeatherLocal] to [WeatherGroup]
     */
    fun map(weatherLocal: List<WeatherLocal>): WeatherGroup = with(weatherLocal) {
        if (isEmpty()) WeatherGroup() else WeatherGroup(first().city, map { map(it) })
    }

}