package com.bushuev.dmitriy.data.weather.local.database

import android.arch.persistence.room.*
import com.bushuev.dmitriy.data.weather.local.entity.WeatherLocal
import io.reactivex.Single

/**
 * Describes base functionality of local weather storage
 *
 * @author Dmitriy Bushuev.
 */
@Dao
interface WeatherDao {

    /**
     * Get weather list from local storage
     */
    @Query("SELECT * FROM Weather")
    fun getWeather(): Single<List<WeatherLocal>>

    /**
     * Clear table of weather and insert new values
     */
    @Transaction
    fun update(weather: List<WeatherLocal>) {
        clear()
        putWeather(weather)
    }

    /**
     * Save list of (@link WeatherLocal) in local storage with replace conflict's resolving strategy
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun putWeather(weather: List<WeatherLocal>)

    /**
     * Clear weather table
     */
    @Query("DELETE FROM Weather")
    fun clear()

}