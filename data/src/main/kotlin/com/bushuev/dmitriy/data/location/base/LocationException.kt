package com.bushuev.dmitriy.data.location.base

/**
 * Base exception of working process with location
 *
 * @author Dmitriy Bushuev
 */
class LocationException : RuntimeException {
    constructor() : super()

    constructor(message: String?) : super(message)

    constructor(message: String?, cause: Throwable?) : super(message, cause)
}