package com.bushuev.dmitriy.data.utils

import java.util.*

/**
 * Converts seconds to [Date]
 *
 * @author Dmitriy Bushuev
 */
class SecondsToDateConverter : Converter<Long, Date> {

    override fun convert(source: Long): Date =
            Date(source * SECONDS_MULTIPLIER)

    companion object {
        private const val SECONDS_MULTIPLIER = 1000
    }

}