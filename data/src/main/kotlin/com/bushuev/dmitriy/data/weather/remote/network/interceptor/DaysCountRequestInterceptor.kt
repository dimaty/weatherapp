package com.bushuev.dmitriy.data.weather.remote.network.interceptor

import com.bushuev.dmitriy.data.ApplicationProperties
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Intercepts all requests to weather api and
 * adds additional information about required response count
 *
 * <p>
 *     There are no requirements to dynamically define count,
 *     that is why all requests come predefined count parameter
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class DaysCountRequestInterceptor(private val applicationProperties: ApplicationProperties) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain!!.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter(COUNT_KEY, applicationProperties.getDaysCount())
                .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
                .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }

    companion object {
        private const val COUNT_KEY = "cnt"
    }

}