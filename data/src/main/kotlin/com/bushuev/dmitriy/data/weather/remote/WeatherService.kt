package com.bushuev.dmitriy.data.weather.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Base functionality of remote weather api
 *
 * @author Dmitriy Bushuev
 */
interface WeatherService {

    /**
     * Request list of weather from openweathermap api
     */
    @GET("data/2.5/forecast/daily")
    fun getWeather(@Query("lat") latitude: Double, @Query("lon") longitude: Double):
            Single<WeatherResponse>

}