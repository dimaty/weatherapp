package com.bushuev.dmitriy.data.weather.remote

import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import io.reactivex.Single

/**
 * Describes functionality of remote weather data source
 *
 * @author Dmitry Bushuev
 */
interface WeatherRemoteDataSource {

    /**
     * Retrieve remote weather data by location [Location]
     */
    fun getWeather(location: Location): Single<WeatherGroup>

}