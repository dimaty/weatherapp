package com.bushuev.dmitriy.data.weather.remote

import com.google.gson.annotations.SerializedName

/**
 * Remote representation of weather entity
 *
 * @author Dmitriy Bushuev
 */
data class WeatherRemote(@SerializedName("dt") val date: Long,
                         @SerializedName("temp") val temperature: Double,
                         @SerializedName("pressure") val pressure: Double,
                         @SerializedName("humidity") val humidity: Double,
                         @SerializedName("speed") val speed: Double,
                         @SerializedName("main") val description: String,
                         @SerializedName("id") val weatherType: Int
)