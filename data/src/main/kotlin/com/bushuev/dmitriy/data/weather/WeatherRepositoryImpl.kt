package com.bushuev.dmitriy.data.weather

import com.bushuev.dmitriy.data.location.base.LocationException
import com.bushuev.dmitriy.data.weather.local.WeatherLocalDataSource
import com.bushuev.dmitriy.data.weather.remote.WeatherRemoteDataSource
import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import com.bushuev.dmitriy.domain.repository.WeatherRepository
import io.reactivex.Single

/**
 * Implementation of [WeatherRepository]
 *
 * <p>
 *     Working logic:
 *          1) Request weather from remote api
 *          2) Success -> save data to local storage and wrap into [Result] class
 *             Error -> requests data from local storage and wrap this data and occurred error into [Result]
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class WeatherRepositoryImpl(private val localDataSource: WeatherLocalDataSource,
                            private val remoteDataSource: WeatherRemoteDataSource)
    : WeatherRepository {

    override fun getWeather(location: Location?): Single<Result<WeatherGroup>> {
        return location?.let {
            remoteDataSource.getWeather(location)
                    .doOnSuccess { localDataSource.putWeather(it) }
                    .map { Result(it) }
                    .onErrorResumeNext { error ->
                        getCachedWeather(error)
                    }
        } ?: getCachedWeather(LocationException("No location defined for weather request"))
    }

    private fun getCachedWeather(error: Throwable) =
            localDataSource.getWeather().map { data -> Result(data, error) }

}
