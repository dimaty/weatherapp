package com.bushuev.dmitriy.data.location

import com.bushuev.dmitriy.data.location.base.RxLocationProvider
import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.Result
import com.bushuev.dmitriy.domain.repository.LocationRepository
import io.reactivex.Single

/**
 * Implementation of [LocationRepository] based on [RxLocationProvider]
 *
 * <p>
 *     If error occurred during finding last known location - returns default location
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class LocationRepositoryImpl(private val locationProvider: RxLocationProvider)
    : LocationRepository {

    override fun getLastLocation(): Single<Result<Location>> =
            locationProvider.lastLocation()
                    .map { Result(Location(it.latitude, it.longitude)) }
                    .onErrorReturn { error -> Result(error = error) }

}
