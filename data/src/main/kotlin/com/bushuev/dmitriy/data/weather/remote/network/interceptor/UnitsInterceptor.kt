package com.bushuev.dmitriy.data.weather.remote.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Intercepts all requests to weather api and adds units query parameter
 *
 * <p>
 *     There are no requirements to dynamically define units,
 *     that is why all requests come with [metric] type of units
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class UnitsInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain!!.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter(UNITS_KEY, UNITS_VALUE)
                .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
                .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }

    companion object {
        private const val UNITS_KEY = "units"
        private const val UNITS_VALUE = "metric"
    }

}