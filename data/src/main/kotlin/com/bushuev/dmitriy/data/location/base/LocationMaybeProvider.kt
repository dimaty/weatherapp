package com.bushuev.dmitriy.data.location.base

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationManager
import io.reactivex.SingleEmitter
import io.reactivex.SingleOnSubscribe

/**
 * Provides location result or error to emitter
 *
 * <p>
 *     Find location in all providers, that are accessible.
 *     If last known location is null(user disabled GPS, or there is no cached location found) -
 *     throw location exception
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class LocationMaybeProvider(private val context: Context) : SingleOnSubscribe<Location> {

    private val locationManager: LocationManager =
            context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    override fun subscribe(emitter: SingleEmitter<Location>) {

        val location = getLastKnownLocation()

        location?.let {
            emitter.onSuccess(it)
        } ?: emitter.onError(LocationException("No location found"))

    }

    /**
     * Find last known best location in all accessible providers
     */
    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(): Location? {
        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l = locationManager.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                bestLocation = l
            }
        }
        return bestLocation
    }

}
