package com.bushuev.dmitriy.data.utils

/**
 * Simple value converter interface
 *
 * @author Dmitriy Bushuev.
 */
interface Converter<in I, out O> {

    /**
     * Converts input value of type [I] to value with type [O]
     */
    fun convert(source: I): O

}