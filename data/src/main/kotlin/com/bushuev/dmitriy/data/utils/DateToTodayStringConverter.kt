package com.bushuev.dmitriy.data.utils

import java.util.*

/**
 * Converts date to string, according to today date
 *
 * <p>
 *     Returns [today] string if input date is today date
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class DateToTodayStringConverter(format: String = DEFAULT_FORMAT, private val today: String)
    : Converter<Date, String> {

    private val delegate: DateToStringConverter = DateToStringConverter(format)

    override fun convert(source: Date): String =
            if (isToday(source)) today else delegate.convert(source)

    /**
     * Check whether input date is today date
     */
    private fun isToday(date: Date): Boolean {
        val today = Calendar.getInstance()
        val target = Calendar.getInstance().apply { time = date }

        return today.get(Calendar.ERA) == target.get(Calendar.ERA) &&
                today.get(Calendar.YEAR) == target.get(Calendar.YEAR) &&
                today.get(Calendar.DAY_OF_YEAR) == target.get(Calendar.DAY_OF_YEAR)
    }

    companion object {
        private const val DEFAULT_FORMAT = "dd.HH.yyy"
    }

}