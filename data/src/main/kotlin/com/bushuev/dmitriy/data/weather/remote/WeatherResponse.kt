package com.bushuev.dmitriy.data.weather.remote

import com.google.gson.annotations.SerializedName

/**
 * Response from remote source of weather data
 *
 * @author Dmiriy Bushuev
 */
data class WeatherResponse(@SerializedName("name") val city: String,
                           @SerializedName("list") val weather: List<WeatherRemote>)