package com.bushuev.dmitriy.data.weather.remote.network

import com.bushuev.dmitriy.data.ApplicationProperties
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * Factory for retrofit services
 *
 * <p>
 *     Required already initialized [OkHttpClient], [Converter.Factory]
 *     and information about base api url
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class ServiceFactory @Inject constructor(private val applicationProperties: ApplicationProperties,
                                         httpClient: OkHttpClient,
                                         converterFactory: Converter.Factory) {

    private val retrofitBuilder: Retrofit.Builder = Retrofit.Builder()
            .client(httpClient)
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

    fun <S : Any> createService(service: KClass<S>): S {
        return retrofitBuilder
                .baseUrl(applicationProperties.getServerUrl())
                .build()
                .create(service.java)
    }

}