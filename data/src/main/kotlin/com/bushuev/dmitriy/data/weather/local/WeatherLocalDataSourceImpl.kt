package com.bushuev.dmitriy.data.weather.local

import com.bushuev.dmitriy.data.weather.WeatherMapper
import com.bushuev.dmitriy.data.weather.local.database.WeatherDao
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import io.reactivex.Single

/**
 * Implementation of [WeatherLocalDataSource] based on [WeatherDao]
 *
 * <p>
 *     Clear weather cache before every save operation
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class WeatherLocalDataSourceImpl(private val weatherStorage: WeatherDao) : WeatherLocalDataSource {

    override fun getWeather(): Single<WeatherGroup> =
            weatherStorage.getWeather().map(WeatherMapper::map)

    override fun putWeather(weather: WeatherGroup) =
            weatherStorage.update(WeatherMapper.map(weather))

}
