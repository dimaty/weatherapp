package com.bushuev.dmitriy.data.weather.local

import com.bushuev.dmitriy.domain.entity.WeatherGroup
import io.reactivex.Single


/**
 * Describes functionality of local weather data source
 *
 * @author Dmitriy Bushuev
 */
interface WeatherLocalDataSource {

    /**
     * Retrieve weather from local cache
     */
    fun getWeather(): Single<WeatherGroup>

    /**
     * Save weather to local cache
     */
    fun putWeather(weather: WeatherGroup)

}