package com.bushuev.dmitriy.data.weather.remote

import com.bushuev.dmitriy.data.weather.WeatherMapper
import com.bushuev.dmitriy.domain.entity.Location
import com.bushuev.dmitriy.domain.entity.WeatherGroup
import io.reactivex.Single

/**
 * Implementation of [WeatherRemoteDataSource] based on [WeatherService]
 *
 * @author Dmitriy Bushuev
 */
class WeatherRemoteDataSourceImpl(private val weatherService: WeatherService)
    : WeatherRemoteDataSource {

    override fun getWeather(location: Location): Single<WeatherGroup> =
            weatherService.getWeather(location.latitude, location.longitude)
                    .map { WeatherGroup(it.city, it.weather.map(WeatherMapper::map)) }

}