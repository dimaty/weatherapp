package com.bushuev.dmitriy.data.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Converts date to string with specific format
 *
 * @author Dmitriy Bushuev
 */
class DateToStringConverter(private val format: String = DEFAULT_FORMAT) : Converter<Date, String> {

    override fun convert(source: Date): String =
            with(SimpleDateFormat(format, Locale.getDefault()))
            {
                format(source)
            }

    companion object {
        private const val DEFAULT_FORMAT = "dd.HH.yyy"
    }

}
