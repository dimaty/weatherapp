package com.bushuev.dmitriy.data

import android.content.Context
import java.io.IOException
import java.io.InputStream
import java.util.*
import javax.inject.Inject

/**
 * Give access to all application properties
 *
 * @author Dmitriy Bushuev.
 */
class ApplicationProperties @Inject constructor(context: Context) {

    private val properties: Properties = Properties()

    init {
        var propertiesStream: InputStream? = null

        try {
            propertiesStream = context.assets.open(PROPERTIES_FILE)
            properties.load(propertiesStream)
        } catch (e: IOException) {
            throw IllegalStateException("Unable to open properties file {$PROPERTIES_FILE}", e)
        } finally {
            if (propertiesStream != null) {
                try {
                    propertiesStream.close()
                } catch (e: IOException) {
                    throw IllegalArgumentException("Error during stream terminating", e)
                }

            }
        }
    }

    fun getServerUrl(): String = properties.getProperty(SERVER_URL)
    fun getApiKey(): String = properties.getProperty(API_KEY)
    fun getDaysCount(): String = properties.getProperty(DAYS_COUNT)

    companion object {
        private const val SERVER_URL = "server_url"
        private const val API_KEY = "api_key"
        private const val DAYS_COUNT = "days_count"

        private const val PROPERTIES_FILE = "application.properties"
    }

}
