package com.bushuev.dmitriy.data.weather.local.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Describes local entity of weather
 *
 * @author Dmitriy Bushuev.
 */
@Entity(tableName = "weather")
data class WeatherLocal(val city: String,
                        @PrimaryKey val date: Long,
                        val temperature: Double,
                        val pressure: Double,
                        val humidity: Double,
                        val speed: Double,
                        val description: String,
                        val weatherType: Int)
