package com.bushuev.dmitriy.data.weather.local.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.bushuev.dmitriy.data.weather.local.entity.WeatherLocal

/**
 * Application's local database
 *
 * <p>
 *     Provides access to all local tables
 * </p>
 *
 * @author Dmitriy Bushuev.
 */
@Database(entities = [WeatherLocal::class], version = 1, exportSchema = false)
abstract class WeatherDatabase : RoomDatabase() {

    /**
     * Provides access to application's weather storage
     */
    abstract fun weatherDao(): WeatherDao

}