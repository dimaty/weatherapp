package com.bushuev.dmitriy.data.weather.remote.network.interceptor

import com.bushuev.dmitriy.data.ApplicationProperties
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Intercepts all requests to weather api and adds additional api key query parameter
 *
 * @author Dmitriy Bushuev
 */
class ApiKeyRequestInterceptor(private val applicationProperties: ApplicationProperties) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain!!.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
                .addQueryParameter(APPID_KEY, applicationProperties.getApiKey())
                .build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
                .url(url)

        val request = requestBuilder.build()
        return chain.proceed(request)
    }

    companion object {
        private const val APPID_KEY = "APPID"
    }

}