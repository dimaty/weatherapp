package com.bushuev.dmitriy.data.weather.remote.network

import com.bushuev.dmitriy.data.weather.remote.WeatherRemote
import com.google.gson.*
import java.lang.reflect.Type

/**
 * Deserializer for array of [WeatherRemote]
 *
 * <p>
 *     Motivation: avoid unnecessary wrapper class
 * </p>
 *
 * @author Dmitriy Bushuev
 */
class WeatherDeserializer : JsonDeserializer<Array<WeatherRemote>> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Array<WeatherRemote> {
        val weatherList = ArrayList<WeatherRemote>()

        val gson = Gson()

        json?.asJsonArray?.forEach {
            weatherList += createWeatherEntity(it.asJsonObject, gson)
        }

        return weatherList.toTypedArray()
    }

    private fun createWeatherEntity(jsonObject: JsonObject, gson: Gson): WeatherRemote {
        val date: Long? = getDate(jsonObject, gson)
        val temperature: Double? = getTemperature(jsonObject, gson)
        val pressure: Double? = getPressure(jsonObject, gson)
        val humidity: Double? = getHumidity(jsonObject, gson)
        val speed: Double? = getSpeed(jsonObject, gson)

        val descriptionJsonObject = jsonObject.get(WEATHER_KEY)?.asJsonArray?.firstOrNull()?.asJsonObject

        val description: String? = getDescription(descriptionJsonObject, gson)
        val type: Int? = getType(descriptionJsonObject, gson)

        return WeatherRemote(date ?: 0,
                temperature ?: 0.0,
                pressure ?: 0.0,
                humidity ?: 0.0,
                speed ?: 0.0,
                description ?: "",
                type ?: -1)
    }

    private fun getDate(jsonObject: JsonObject, gson: Gson) =
            gson.fromJson(jsonObject.getAsJsonPrimitive(DATE_KEY), Long::class.java)

    private fun getTemperature(jsonObject: JsonObject, gson: Gson) =
            gson.fromJson(jsonObject.getAsJsonObject(TEMPERATURE_KEY)?.get(TEMPERATURE_DAY_KEY), Double::class.java)

    private fun getPressure(jsonObject: JsonObject, gson: Gson) =
            gson.fromJson(jsonObject.getAsJsonPrimitive(PRESSURE_KEY), Double::class.java)

    private fun getHumidity(jsonObject: JsonObject, gson: Gson) =
            gson.fromJson(jsonObject.getAsJsonPrimitive(HUMIDITY_KEY), Double::class.java)

    private fun getSpeed(jsonObject: JsonObject, gson: Gson) =
            gson.fromJson(jsonObject.getAsJsonPrimitive(SPEED_KEY), Double::class.java)

    private fun getDescription(jsonObject: JsonObject?, gson: Gson) =
            gson.fromJson(jsonObject?.getAsJsonPrimitive(MAIN_KEY), String::class.java)

    private fun getType(jsonObject: JsonObject?, gson: Gson) =
            gson.fromJson(jsonObject?.getAsJsonPrimitive(TYPE_KEY), Int::class.java)

    companion object {
        private const val DATE_KEY = "dt"
        private const val TEMPERATURE_KEY = "temp"
        private const val TEMPERATURE_DAY_KEY = "day"
        private const val PRESSURE_KEY = "pressure"
        private const val HUMIDITY_KEY = "humidity"
        private const val SPEED_KEY = "speed"

        private const val WEATHER_KEY = "weather"
        private const val MAIN_KEY = "main"
        private const val TYPE_KEY = "id"
    }

}