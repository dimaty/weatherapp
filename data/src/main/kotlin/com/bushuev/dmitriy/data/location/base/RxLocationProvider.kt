package com.bushuev.dmitriy.data.location.base

import android.content.Context
import android.location.Location
import io.reactivex.Single

/**
 * Contains methods for accessing location information
 *
 * @author Dmitriy Bushuev
 */
class RxLocationProvider(private val context: Context) {

    /**
     * Returns [Single] with information about last known location
     */
    fun lastLocation(): Single<Location> =
            Single.create(LocationMaybeProvider(context))

}