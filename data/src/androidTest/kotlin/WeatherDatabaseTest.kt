import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.bushuev.dmitriy.data.weather.local.database.WeatherDao
import com.bushuev.dmitriy.data.weather.local.database.WeatherDatabase
import com.bushuev.dmitriy.data.weather.local.entity.WeatherLocal
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * [WeatherDao] test cases
 *
 * @author Dmitriy Bushuev
 */
@RunWith(AndroidJUnit4::class)
class WeatherDatabaseTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var weatherDatabase: WeatherDatabase

    @Before
    fun initDb() {
        weatherDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                WeatherDatabase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @After
    fun closeDb() {
        weatherDatabase.close()
    }

    @Test
    fun putGetDataTest() {
        val weatherDao = weatherDatabase.weatherDao()

        weatherDao.clear()
        weatherDao.putWeather(getTestList())

        // Data in table should be not empty after putting
        weatherDao.getWeather()
                .test()
                .assertValue { !it.isEmpty() }
    }

    @Test
    fun clearDataTest() {
        val weatherDao = weatherDatabase.weatherDao()

        weatherDao.putWeather(getTestList())
        weatherDao.clear()

        // Data in table should be empty after clearing
        weatherDao.getWeather()
                .test()
                .assertValue { it.isEmpty() }
    }

    @Test
    fun updateDataTest() {
        val weatherDao = weatherDatabase.weatherDao()

        val data = getTestList()

        weatherDao.clear()
        weatherDao.putWeather(data)

        // Put some data in table before updating
        weatherDao.getWeather()
                .test()
                .assertValue { !it.isEmpty() }

        weatherDao.update(getTestList())

        // Update should clear all previous items before setting new
        weatherDao.getWeather()
                .test()
                .assertValue { it.size == data.size }
    }

    private fun getTestList() =
            listOf(
                    WeatherLocal("Minsk", 2353425, 2.0, 3.0, 4.0, 13.2, "test1", 1),
                    WeatherLocal("Borisov", 2353426, 2.0, 3.0, 4.0, 13.2, "test1", 1),
                    WeatherLocal("Zhodino", 2353427, 2.0, 3.0, 4.0, 13.2, "test1", 1),
                    WeatherLocal("Baranovichi", 2353428, 2.0, 3.0, 4.0, 13.2, "test1", 1)
            )

}
