package com.bushuev.dmitriy.buildsrc

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

/**
 * Task for expanding project properties map by template in {@code sourceAssets} to target directory {@code targetAssets}
 *
 * @author Dmitriy Bushuev.
 */
class ExpandPropertiesTask extends DefaultTask {

    @InputDirectory
    File sourceAssets

    @OutputDirectory
    File targetAssets

    @TaskAction
    void expand() {
        targetAssets.mkdirs()
        targetAssets.delete()

        // delete previous version
        project.delete(targetAssets)

        // copy expandable files
        project.copy {
            it.from(sourceAssets)
            it.into(targetAssets)
            it.filteringCharset = 'UTF-8'
            it.include('**/*.properties')
            it.expand(project.properties)
        }
    }

}
